<?php
/**
 * @file
 * Admin form for search api history views module.
 */

/**
 * Form constructor for the search_api_history_views settings form.
 *
 * This form allows to configure which views to check for search history.
 *
 * @see search_api_history_views_admin_form_submit()
 * @ingroup forms
 */
function search_api_history_views_admin_form($form, &$form_state) {
  $views = views_get_enabled_views();
  $options = array();
  foreach ($views as $name => $view) {
    $human_name = $view->human_name ? $view->human_name : $view->name;
    $options[$name] = $human_name;
  }
  $form['views'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => variable_get('search_api_history_views', array()),
    '#title' => t('Select views to be checked by search history.'),
  );

  $form['default_index_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Default index name'),
    '#default_value' => variable_get('sapihv_default_index_name', 'Keyword'),
    '#description' => t('Index name which will replace empty fields name'),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

/**
 * Form submit handler for search_api_history_views_admin_form().
 */
function search_api_history_views_admin_form_submit($form, &$form_state) {
  $selected = array_filter($form_state['values']['views']);
  $default_index_name = $form_state['values']['default_index_name'];
  variable_set('search_api_history_views', $selected);
  variable_set('sapihv_default_index_name', $default_index_name);
}
